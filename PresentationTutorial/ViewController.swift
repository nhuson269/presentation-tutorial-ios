//
//  ViewController.swift
//  PresentationTutorial
//
//  Created by Nhu Son on 7/9/18.
//  Copyright © 2018 Nhu Son. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    @IBAction func handlePerformSegueButtonClicked(_ sender: Any) {
        self.performSegue(withIdentifier: "ToPresentedVCSegue", sender: self)
    }
    
    var slideInPresentationDelegate = SlideInPresentationDelegate()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ToPresentedVCSegue" {
            let direction = PresentationDirection(rawValue: segmentedControl.selectedSegmentIndex)!
            slideInPresentationDelegate.direction = direction
            
            let controller = segue.destination
            controller.transitioningDelegate = slideInPresentationDelegate
            controller.modalPresentationStyle = .custom
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

